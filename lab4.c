#include <stdio.h>

int main()
{
    int n,sum=0,temp;

    printf("Enter a number:\n");
    scanf("%d",&n);

    temp=n;

    while(temp!=0)
    {
        int a=temp%10;
        temp/=10;
        sum+=a;
    }

    printf("Sum of digits of %d is: %d\n",n,sum);

    return 0;
}

