#include <stdio.h>

int main()
{
    int a,b,c;

    printf("Enter the three numbers:\n");
    scanf("%d,%d,%d",&a,&b,&c);

    if(b>a)
    {
        a=b;
    }

    if(c>a)
    {
        a=c;
    }

    printf("The largest of three numbers is: %d\n",a);
    return 0;
}